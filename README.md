# BRCloud development repository

This repository is used to merge automatically the new commits of the upstream
LLVM project every night. It also contains the Dockerfile used to create the
image ´brcloudproject/llvm-builder:latest´ used by LLVM building jobs: this
image is built automatically by docker hub after any change.
